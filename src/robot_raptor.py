import sys
import pygame
import pygame.camera
from pygame.locals import *
import RPi.GPIO as GPIO
import time
import datetime
import os
import glob

# Initialisation de pygame
pygame.init()
pygame.camera.init()

# Création d'une fentre
screen = pygame.display.set_mode()
# Titre
pygame.display.set_caption("Drone d'exploration terrestre : Raptor")
#rectScreen = screen.get_rect()

# Création de l'objet camera
cam_list = pygame.camera.list_cameras()
cam = pygame.camera.Camera(cam_list[0],(640,480))
cam.start()

# Icon
image = pygame.image.load("../image/téléchargement.png")
pygame.display.set_icon(image)

# Police d'ecriture
police = pygame.font.Font(None,50)
police_texte = pygame.font.Font(None,30)

# Récuperation de la date et de l'heure de connection
date = datetime.datetime.now()

# Different texte pour l'interface
texte_date = police_texte.render(str(date),True,pygame.Color(0,200,0))
texte_date_titre = police_texte.render("Connection : ",True,pygame.Color(0,200,0))
Titre = police.render("Exploration raptor",True,pygame.Color(0,200,0))
signature = police_texte.render("by : Theophile Yvars",True,pygame.Color(0,200,0))
temperature_titre = police_texte.render("Temperature C° : ",True,pygame.Color(0,200,0))

#rectTexte = texte.get_rect()
#rectTexte.center = rectScreen.center

# 2)initialisation des E/S

# 2.1) choix du brochage GPIO

GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)

# 2.2) attribution des E/S

Moteur_1_avant = 33
Moteur_1_arriere = 35
Moteur_2_avant = 12
Moteur_2_arriere = 32

# 2.3) etat des E/S au demarrage

GPIO.setup(Moteur_1_avant, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(Moteur_1_arriere, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(Moteur_2_avant, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(Moteur_2_arriere, GPIO.OUT, initial=GPIO.LOW)


# PWM

pwm1 = GPIO.PWM(Moteur_1_avant, 50)  # pwm à une fréquence de 50 Hz
pwm2 = GPIO.PWM(Moteur_1_arriere, 50)  # pwm à une fréquence de 50 Hz
pwm3 = GPIO.PWM(Moteur_2_avant, 50)  # pwm à une fréquence de 50 Hz
pwm4 = GPIO.PWM(Moteur_2_arriere, 50)  # pwm à une fréquence de 50 Hz
rapport = 50       # rapport cyclique initial de 7%
pwm1.start(rapport)
pwm2.start(rapport)
pwm3.start(rapport)
pwm4.start(rapport) 

#Capteur de Temperature

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')
base_dir = '/sys/bus/w1/devices/'
device_folder = glob.glob(base_dir + '28*')[0]
device_file = device_folder + '/w1_slave'

#fonction temperature

def read_temp_raw():
    f = open(device_file, 'r')
    lines = f.readlines()
    f.close()
    return lines

def read_temp():
    lines = read_temp_raw()
    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = read_temp_raw()
    equals_pos = lines[1].find('t=')
    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0
        temp_f = temp_c * 9.0 / 5.0 + 32.0
        return temp_c

# Affichage de la température
temperature = police_texte.render(str(read_temp()),True,pygame.Color(0,200,0))

# 2.4)creation des fonctions pour le movement du robot

def avant() :
    print ("marche avant")
    #GPIO.output(Moteur_1_arriere, GPIO.LOW)
    #GPIO.output(Moteur_2_arriere, GPIO.LOW)
    #GPIO.output(Moteur_1_avant, GPIO.HIGH)
    #GPIO.output(Moteur_2_avant, GPIO.HIGH)
    pwm1.ChangeDutyCycle(float(rapport))
    pwm2.ChangeDutyCycle(0)
    pwm3.ChangeDutyCycle(float(rapport))
    pwm4.ChangeDutyCycle(0)


def arriere() :
    #print ("marche arriere")
    #GPIO.output(Moteur_1_avant, GPIO.LOW)
    #GPIO.output(Moteur_2_avant, GPIO.LOW)
    #GPIO.output(Moteur_1_arriere, GPIO.HIGH)
    #GPIO.output(Moteur_2_arriere, GPIO.HIGH)
    #time.sleep(0.5)
    pwm1.ChangeDutyCycle(0)
    pwm2.ChangeDutyCycle(float(rapport))
    pwm3.ChangeDutyCycle(0)
    pwm4.ChangeDutyCycle(float(rapport))

def droite() :
    print ("droite")
    #GPIO.output(Moteur_1_avant, GPIO.LOW)
    #GPIO.output(Moteur_2_arriere, GPIO.LOW)
    #GPIO.output(Moteur_1_arriere, GPIO.HIGH)
    #GPIO.output(Moteur_2_avant, GPIO.HIGH)
    #time.sleep(0.5)
    pwm1.ChangeDutyCycle(0)
    pwm2.ChangeDutyCycle(float(rapport))
    pwm3.ChangeDutyCycle(float(rapport))
    pwm4.ChangeDutyCycle(0)

def gauche() :
    print ("gauche")
    GPIO.output(Moteur_1_arriere, GPIO.LOW)
    GPIO.output(Moteur_2_avant, GPIO.LOW)
    GPIO.output(Moteur_1_avant, GPIO.HIGH)
    GPIO.output(Moteur_2_arriere, GPIO.HIGH)
   # time.sleep(0.5)
    pwm1.ChangeDutyCycle(float(rapport))
    pwm2.ChangeDutyCycle(0)
    pwm3.ChangeDutyCycle(0)
    pwm4.ChangeDutyCycle(float(rapport))

def stop() :
    #GPIO.output(Moteur_1_avant, GPIO.LOW)
    #GPIO.output(Moteur_1_arriere, GPIO.LOW)
    #GPIO.output(Moteur_2_avant, GPIO.LOW)
    #GPIO.output(Moteur_2_arriere, GPIO.LOW)
    pwm1.ChangeDutyCycle(0)
    pwm2.ChangeDutyCycle(0)
    pwm3.ChangeDutyCycle(0)
    pwm4.ChangeDutyCycle(0)

# Blit des images sur la fentre

screen.blit(Titre,(750,20))
screen.blit(signature,(700+120,800))
screen.blit(texte_date,(750+120,700))
screen.blit(texte_date_titre,(600+120,700))
screen.blit(temperature_titre,(600+120,675))
screen.blit(temperature,(600+300,675))

# Boucle Main

while True:
    # affichage de la camera dans la fentre
   image1 = cam.get_image()
   image1 = pygame.transform.scale(image1,(640,480))
   screen.blit(image1,(600,100))
   pygame.display.update()
   pygame.display.flip()
   #screen.fill(pygame.Color("#FF0000"))

    # Evenement suivant les touches
   for event in pygame.event.get():
   #for event in pygame.event.wait():
        if event.type == pygame.QUIT:
            cam.stop()
            pygame.quit()
            sys.exit()
        if event.type==KEYDOWN:
            if event.key==K_ESCAPE:
                cam.stop()
                pygame.quit()
                sys.exit()
            if event.key==K_t:
                screen.fill((0,0,0))
                screen.blit(Titre,(750,20))
                screen.blit(signature,(700+120,800))
                screen.blit(texte_date,(750+120,700))
                screen.blit(texte_date_titre,(600+120,700))
                temperature = police_texte.render(str(read_temp()),True,pygame.Color(0,200,0))
                screen.blit(temperature_titre,(600+120,675))
                screen.blit(temperature,(600+300,675))
                pygame.display.flip
            if event.key==K_a:
                if rapport < 100:
                    rapport += 10
            if event.key==K_q:
                if rapport > 30:
                    rapport -= 10
            if event.key==K_UP:
                avant()
     #           stop()
            if event.key==K_DOWN:
                arriere()
      #          stop()
            if event.key==K_LEFT:
                gauche()
       #         stop()
            if event.key==K_RIGHT:
                print ("droite")
                droite()
        #        stop()
            if event.key==K_x:
                print ("stop")
         #       stop()
            if event.key==K_ESCAPE:
                print ("bye bye")
                GPIO.output(Moteur_1_avant, GPIO.LOW)
                GPIO.output(Moteur_1_arriere, GPIO.LOW)
                GPIO.output(Moteur_2_avant, GPIO.LOW)
                GPIO.output(Moteur_2_arriere, GPIO.LOW)
                a=0
        else:
            stop()
   #pygame.display.update()
   #pgame.display.flip()
   pygame.display.flip
