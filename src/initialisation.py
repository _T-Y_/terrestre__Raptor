import pygame
import pygame.camera
import datetime

# Initialisation de pygame
pygame.init()
pygame.camera.init()
print("\nInitialisation : OK")
    
# Création d'une fentre
screen = pygame.display.set_mode()
# Titre
pygame.display.set_caption("Drone d'exploration terrestre : Raptor")
#rectScreen = screen.get_rect()

# Icon
icon = pygame.image.load("../image/ic.png")
#image = pygame.image.load("../image/téléchargement.png")
pygame.display.set_icon(icon)
# Police d'ecriture
police = pygame.font.Font(None,50)
police_texte = pygame.font.Font(None,30)
print("Window create : OK")

# Récuperation de la date et de l'heure de connection
date = datetime.datetime.now()

# Different texte pour l'interface
texte_date = police_texte.render(str(date),True,pygame.Color(0,200,0))
texte_date_titre = police_texte.render("Connection : ",True,pygame.Color(0,200,0))
Titre = police.render("Exploration raptor",True,pygame.Color(0,200,0))
signature = police_texte.render("by : Theophile Yvars",True,pygame.Color(0,200,0))
temperature_titre = police_texte.render("Temperature C° : ",True,pygame.Color(0,200,0))


