from PIL import ImageOps

import pygame
import pygame.camera
from pygame.locals import *
import sys

import temperature_sensor
import camera
import camera_direction
import motor
import initialisation
import led


# Blit des images sur la fentre

initialisation.screen.blit(initialisation.Titre,(750,20))
initialisation.screen.blit(initialisation.signature,(700+120,800))
initialisation.screen.blit(initialisation.texte_date,(750+120,700))
initialisation.screen.blit(initialisation.texte_date_titre,(600+120,700))
initialisation.screen.blit(initialisation.temperature_titre,(600+120,675))
initialisation.screen.blit(temperature_sensor.temperature,(600+300,675))
print("Graphic : ok")

camera_direction.setup()

led.ledInit()

# Boucle Main

while True:
    # affichage de la camera dans la fentre
   image = camera.cam.get_image()
   image = pygame.transform.scale(image,(640,480))

   imageRotation = camera.rot_center(image, 180)
   
   initialisation.screen.blit(imageRotation,(600,100))
   pygame.display.update()
   pygame.display.flip()
   #screen.fill(pygame.Color("#FF0000"))

# Evenement suivant les touches
   for event in pygame.event.get():
        if event.type == pygame.QUIT:
            camera.cam.stop()
            pygame.quit()
            sys.exit()
        if event.type==KEYDOWN:
            if event.key==K_ESCAPE:
                camera.cam.stop()
                pygame.quit()
                sys.exit()
            if event.key==K_w:
                led.ledRouge()
            if event.key==K_x:
                led.ledBleu()
            if event.key==K_c:
                led.ledVerte()
            if event.key==K_v:
                led.ledStop()
            if event.key==K_m:
                camera_direction.forwardStep()
            if event.key==K_p:
                camera_direction.backwardStep()
            if event.key==K_l:
                for i in range(10):
                    camera_direction.forwardStep()
            if event.key==K_o:
                for i in range(10):
                    camera_direction.backwardStep()
            if event.key==K_t:
                initialisation.screen.fill((0,0,0))
                initialisation.screen.blit(initialisation.Titre,(750,20))
                initialisation.screen.blit(initialisation.signature,(700+120,800))
                initialisation.screen.blit(initialisation.texte_date,(750+120,700))
                initialisation.screen.blit(initialisation.texte_date_titre,(600+120,700))
                temperature_sensor.temperature = initialisation.police_texte.render(str(temperature_sensor.read_temp()),True,pygame.Color(0,200,0))
                initialisation.screen.blit(initialisation.temperature_titre,(600+120,675))
                initialisation.screen.blit(temperature_sensor.temperature,(600+300,675))
                pygame.display.flip
            if event.key==K_a:
                if motor.rapport < 100:
                    motor.rapport += 10
            if event.key==K_q:
                if motor.rapport > 30:
                    motor.rapport -= 10
            if event.key==K_UP:
                motor.avant()
            if event.key==K_DOWN:
                motor.arriere()
            if event.key==K_LEFT:
                motor.gauche()
            if event.key==K_RIGHT:
                print ("droite")
                motor.droite()
        else:
            motor.stop()
   pygame.display.flip
