import RPi.GPIO as GPIO
# 2)initialisation des E/S

# 2.1) choix du brochage GPIO

GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)

# 2.2) attribution des E/S

Moteur_1_avant = 35
Moteur_1_arriere = 33
Moteur_2_avant = 32
Moteur_2_arriere = 12

# 2.3) etat des E/S au demarrage

GPIO.setup(Moteur_1_avant, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(Moteur_1_arriere, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(Moteur_2_avant, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(Moteur_2_arriere, GPIO.OUT, initial=GPIO.LOW)


# PWM

pwm1 = GPIO.PWM(Moteur_1_avant, 50)  # pwm à une fréquence de 50 Hz
pwm2 = GPIO.PWM(Moteur_1_arriere, 50)  # pwm à une fréquence de 50 Hz
pwm3 = GPIO.PWM(Moteur_2_avant, 50)  # pwm à une fréquence de 50 Hz
pwm4 = GPIO.PWM(Moteur_2_arriere, 50)  # pwm à une fréquence de 50 Hz
rapport = 50       # rapport cyclique initial de 7%
pwm1.start(rapport)
pwm2.start(rapport)
pwm3.start(rapport)
pwm4.start(rapport) 

print("Motor : ok")

reglage = 20

def avant() :
    print ("marche avant")
    #GPIO.output(Moteur_1_arriere, GPIO.LOW)
    #GPIO.output(Moteur_2_arriere, GPIO.LOW)
    #GPIO.output(Moteur_1_avant, GPIO.HIGH)
    #GPIO.output(Moteur_2_avant, GPIO.HIGH)
    pwm1.ChangeDutyCycle(float(rapport-reglage))
    pwm2.ChangeDutyCycle(0)
    pwm3.ChangeDutyCycle(float(rapport))
    pwm4.ChangeDutyCycle(0)


def arriere() :
    #print ("marche arriere")
    #GPIO.output(Moteur_1_avant, GPIO.LOW)
    #GPIO.output(Moteur_2_avant, GPIO.LOW)
    #GPIO.output(Moteur_1_arriere, GPIO.HIGH)
    #GPIO.output(Moteur_2_arriere, GPIO.HIGH)
    #time.sleep(0.5)
    pwm1.ChangeDutyCycle(0)
    pwm2.ChangeDutyCycle(float(rapport))
    pwm3.ChangeDutyCycle(0)
    pwm4.ChangeDutyCycle(float(rapport - reglage))

def gauche() :
    print ("gauche")
    #GPIO.output(Moteur_1_avant, GPIO.LOW)
    #GPIO.output(Moteur_2_arriere, GPIO.LOW)
    #GPIO.output(Moteur_1_arriere, GPIO.HIGH)
    #GPIO.output(Moteur_2_avant, GPIO.HIGH)
    #time.sleep(0.5)
    pwm1.ChangeDutyCycle(0)
    pwm2.ChangeDutyCycle(float(rapport))
    pwm3.ChangeDutyCycle(float(rapport))
    pwm4.ChangeDutyCycle(0)

def droite() :
    print ("droite")
    #GPIO.output(Moteur_1_arriere, GPIO.LOW)
    #GPIO.output(Moteur_2_avant, GPIO.LOW)
    #GPIO.output(Moteur_1_avant, GPIO.HIGH)
    #GPIO.output(Moteur_2_arriere, GPIO.HIGH)
   # time.sleep(0.5)
    pwm1.ChangeDutyCycle(float(rapport))
    pwm2.ChangeDutyCycle(0)
    pwm3.ChangeDutyCycle(0)
    pwm4.ChangeDutyCycle(float(rapport))

def stop() :
    #GPIO.output(Moteur_1_avant, GPIO.LOW)
    #GPIO.output(Moteur_1_arriere, GPIO.LOW)
    #GPIO.output(Moteur_2_avant, GPIO.LOW)
    #GPIO.output(Moteur_2_arriere, GPIO.LOW)
    pwm1.ChangeDutyCycle(0)
    pwm2.ChangeDutyCycle(0)
    pwm3.ChangeDutyCycle(0)
    pwm4.ChangeDutyCycle(0)
