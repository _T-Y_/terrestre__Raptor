#!/usr/bin/env python3
#-- coding: utf-8 --

import RPi.GPIO as GPIO #Importe la bibliothèque pour contrôler les GPIOs

LedRouge = 16 #Définit le numéro du port GPIO qui alimente la led
LedBleu = 18 #Définit le numéro du port GPIO qui alimente la led
LedVerte = 22 #Définit le numéro du port GPIO qui alimente la led

def ledInit():
    GPIO.setmode(GPIO.BOARD) #Définit le mode de numérotation (Board)
    GPIO.setwarnings(False) #On désactive les messages d'alerte
    GPIO.setup(LedRouge, GPIO.OUT) #Active le contrôle du GPIO
    GPIO.setup(LedBleu, GPIO.OUT) #Active le contrôle du GPIO
    GPIO.setup(LedVerte, GPIO.OUT) #Active le contrôle du GPIO
    GPIO.output(LedRouge, GPIO.LOW)
    GPIO.output(LedBleu, GPIO.LOW)
    GPIO.output(LedVerte, GPIO.LOW)
    print("Leds : ok")
    
def ledRouge():
    GPIO.output(LedRouge, GPIO.HIGH) #On l'allume
    print("Led rouge : Allumé")
    
def ledBleu():
    GPIO.output(LedBleu, GPIO.HIGH) #On l'allume
    print("Led bleu : Allumé")

def ledVerte():
    GPIO.output(LedVerte, GPIO.HIGH) #On l'allume
    print("Led verte : Allumé")

def ledStop():
    GPIO.output(LedRouge, GPIO.LOW)
    GPIO.output(LedBleu, GPIO.LOW)
    GPIO.output(LedVerte, GPIO.LOW)
    print("Leds : Etteintes")
    
#state = GPIO.input(LED) #Lit l'état actuel du GPIO, vrai si allumé, faux si éteint

#if state : #Si GPIO allumé
 #   GPIO.output(LED, GPIO.LOW) #On l’éteint
#else : #Sinon
  #  GPIO.output(LED, GPIO.HIGH) #On l'allume